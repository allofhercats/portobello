enum TokenKind {
    NewSegment,
    Text,
    HeadingPrefix,
    Bold,
    Italic,
    Underline,
    Strikethrough,
    Monospace,
    MacroPrefix,
    Symbol,
    Comma,
    LParen,
    RParen,
    Color,
    Number,
    String,
    KWTrue,
    KWFalse,
    CodeBlock,
    Formula,
    TableSeparator,
    Link,
    RawHTML,
    BulletListPrefix,
    NumberedListPrefix,
    TodoListPrefix,
    QuotePrefix
}

export {
    TokenKind
};
