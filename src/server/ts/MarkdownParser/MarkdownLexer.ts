import { TokenKind } from "../Enums";
import Token from "./Token";

class MarkdownLexer {
    public constructor(
        private readonly text: string,
        private offset: number = 0,
        private line: number = 1,
        private column: number = 1
    ) {}

    private getCurrentChar(): string {
        if (this.offset < this.text.length) {
            return this.text[this.offset];
        } else {
            return "";
        }
    }

    private getNextChar(): string {
        if (this.offset + 1 < this.text.length) {
            return this.text[this.offset + 1];
        } else {
            return "";
        }
    }

    private eatNextChar(n: number = 1): void {
        if (n <= 0) {
            throw new Error(`Cannot eat non-positive number of characters: ${n}`);
        }

        this.offset += n;
    }

    private areMoreChars(): boolean {
        return this.offset < this.text.length;
    }

    private lexNext(): TokenKind | null {
        if (this.getCurrentChar() === "\n") {
            if (this.getNextChar() === "\n") {
                this.eatNextChar(2);
                return TokenKind.NewSegment;
            } else {
                this.eatNextChar();
                return null;
            }
        } else {
            this.eatNextChar();
            return null;
        }
    }

    public lex(): Token[] {
        let tokens: Token[] = [];

        while (this.areMoreChars()) {
            this.lexNext();
        }

        return tokens;
    }
}

export default MarkdownLexer;
