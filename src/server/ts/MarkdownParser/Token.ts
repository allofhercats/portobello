import { TokenKind } from "../Enums";

class Token {
    public constructor(
        public readonly offset: Number,
        public readonly line: Number,
        public readonly column: Number,
        public readonly text: String,
        public readonly kind: TokenKind
    ) {}
}

export default Token;
