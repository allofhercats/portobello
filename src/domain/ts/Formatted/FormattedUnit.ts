import FormattedBase from "./FormattedBase";
import FormattedSegmentBase from "./FormattedSegmentBase";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedUnit extends FormattedBase {
    constructor(
        public readonly segments: FormattedSegmentBase[] = []
    ) {
        super();
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {
        this.segments.forEach(i => i.accept(visitor));
    }
}

export default FormattedUnit;
