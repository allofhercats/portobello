import { FormattedSegmentListKind } from "../Enums";
import FormattedElementBase from "./FormattedElementBase";
import FormattedSegmentListItemBase from "./FormattedSegmentListItemBase";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedSegmentListItemNode extends FormattedSegmentListItemBase {
    public constructor(
        public kind: FormattedSegmentListKind,
        value: Boolean | null = null,
        public elements: FormattedElementBase[] = [],
        public children: FormattedSegmentListItemBase[] = []
    ) {
        super(value);
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {
        this.elements.forEach(i => i.accept(visitor));
        this.children.forEach(i => i.accept(visitor));
    }
};

export default FormattedSegmentListItemNode;
