import FormattedBase from "./FormattedBase";

abstract class FormattedInlineBase extends FormattedBase {}

export default FormattedInlineBase;
