import FormattedVisitorBase from "./FormattedVisitorBase";

abstract class FormattedBase {
    protected abstract onAccept(
        visitor: FormattedVisitorBase
    ): void;

    public accept(
        visitor: FormattedVisitorBase
    ): void {
        this.onAccept(visitor);
        visitor.visit(this);
    }
}

export default FormattedBase;
