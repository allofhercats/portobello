import FormattedElementStringBase from "./FormattedElementStringBase";
import { FormattedElementStringSource } from "../Enums";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedElementStringCode extends FormattedElementStringBase {
    public constructor(
        source: FormattedElementStringSource,
        string: String,
        public syntax: String | null = null
    ) {
        super(source, string);
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {}
};

export default FormattedElementStringCode;
