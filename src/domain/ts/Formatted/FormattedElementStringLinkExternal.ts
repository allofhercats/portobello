import FormattedElementStringBase from "./FormattedElementStringBase";
import { FormattedElementStringSource } from "../Enums";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedElementStringLinkExternal extends FormattedElementStringBase {
    public stringURLEvaluated: String | null;

    public constructor(
        source: FormattedElementStringSource,
        string: String,
        public sourceURL: FormattedElementStringSource,
        public stringURL: String
    ) {
        super(source, string);

        this.stringURLEvaluated = null;
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {}
};

export default FormattedElementStringLinkExternal;
