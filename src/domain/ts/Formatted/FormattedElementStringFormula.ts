import Color from "color";
import FormattedElementStringBase from "./FormattedElementStringBase";
import { FormattedElementStringSource } from "../Enums";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedElementStringFormula extends FormattedElementStringBase {
    public constructor(
        source: FormattedElementStringSource,
        string: String,
        public colorForeground: Color | null = null,
        public colorBackground: Color | null = null
    ) {
        super(source, string);
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {}
};

export default FormattedElementStringFormula;
