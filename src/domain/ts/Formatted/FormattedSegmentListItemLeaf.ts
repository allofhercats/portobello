import FormattedElementBase from "./FormattedElementBase";
import FormattedSegmentListItemBase from "./FormattedSegmentListItemBase";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedSegmentListItemLeaf extends FormattedSegmentListItemBase {
    public constructor(
        value: Boolean | null = null,
        public elements: FormattedElementBase[] = []
    ) {
        super(value);
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {
        this.elements.forEach(i => i.accept(visitor));
    }
};

export default FormattedSegmentListItemLeaf;
