import FormattedSegmentBase from "./FormattedSegmentBase";
import FormattedSegmentListItemBase from "./FormattedSegmentListItemBase";
import FormattedElementBase from "./FormattedElementBase";
import FormattedVisitorBase from "./FormattedVisitorBase";
import { FormattedSegmentListKind } from "../Enums";

class FormattedSegmentList extends FormattedSegmentBase {
    public constructor(
        public kind: FormattedSegmentListKind,
        public children: FormattedSegmentListItemBase[] = []
    ) {
        super();
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {
        this.children.forEach(i => i.accept(visitor));
    }
};

export default FormattedSegmentList;
