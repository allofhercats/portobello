import FormattedElementBase from "./FormattedElementBase";
import { FormattedElementStringSource } from "../Enums";
import FormattedVisitorBase from "./FormattedVisitorBase";

abstract class FormattedElementStringBase extends FormattedElementBase {
    public stringEvaluated: String | null;

    public constructor(
        public source: FormattedElementStringSource,
        public string: String
    ) {
        super();

        this.stringEvaluated = null;
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {}
}

export default FormattedElementStringBase;
