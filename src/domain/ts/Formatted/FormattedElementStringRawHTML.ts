import FormattedElementStringBase from "./FormattedElementStringBase";
import { FormattedElementStringSource } from "../Enums";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedElementStringRawHTML extends FormattedElementStringBase {
    public constructor(
        source: FormattedElementStringSource,
        string: String
    ) {
        super(source, string);
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {}
};

export default FormattedElementStringRawHTML;
