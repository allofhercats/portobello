import FormattedSegmentBase from "./FormattedSegmentBase";
import { Alignment } from "../Enums";
import FormattedElementBase from "./FormattedElementBase";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedSegmentText extends FormattedSegmentBase {
    public constructor(
        public elements: FormattedElementBase[] = [],
        public alignment: Alignment = Alignment.Left
    ) {
        super();
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {
        this.elements.forEach(i => i.accept(visitor));
    }
}

export default FormattedSegmentText;
