import FormattedSegmentBase from "./FormattedSegmentBase";
import FormattedElementBase from "./FormattedElementBase";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedSegmentHeading extends FormattedSegmentBase {
    public constructor(
        public elements: FormattedElementBase[] = [],
        public level: Number = 1
    ) {
        super();
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {
        this.elements.forEach(i => i.accept(visitor));
    }
}

export default FormattedSegmentHeading;
