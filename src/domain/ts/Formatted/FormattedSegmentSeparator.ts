import FormattedSegmentBase from "./FormattedSegmentBase";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedSegmentSeparator extends FormattedSegmentBase {
    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {}
};

export default FormattedSegmentSeparator;
