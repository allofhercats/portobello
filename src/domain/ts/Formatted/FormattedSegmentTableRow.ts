import FormattedBase from "./FormattedBase";
import FormattedElementBase from "./FormattedElementBase";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedSegmentTableRow extends FormattedBase {
    public constructor(
        public entriesElements: FormattedElementBase[][] = [],
        public height: Number | null = null
    ) {
        super();
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {
        this.entriesElements.forEach(i => i.forEach(j => j.accept(visitor)));
    }
}

export default FormattedSegmentTableRow;
