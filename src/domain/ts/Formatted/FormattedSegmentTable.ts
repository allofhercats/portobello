import FormattedSegmentBase from "./FormattedSegmentBase";
import FormattedSegmentTableColumn from "./FormattedSegmentTableColumn";
import FormattedSegmentTableRow from "./FormattedSegmentTableRow";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedSegmentTable extends FormattedSegmentBase {
    public constructor(
        public columns: FormattedSegmentTableColumn[] = [],
        public rows: FormattedSegmentTableRow[] = []
    ) {
        super();
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {
        this.columns.forEach(i => i.accept(visitor));
        this.rows.forEach(i => i.accept(visitor));
    }
};

export default FormattedSegmentTable;
