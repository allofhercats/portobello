import FormattedElementStringBase from "./FormattedElementStringBase";
import { FormattedElementStringSource } from "../Enums";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedElementStringImage extends FormattedElementStringBase {
    public constructor(
        source: FormattedElementStringSource,
        string: String
    ) {
        super(source, string);
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {}
};

export default FormattedElementStringImage;
