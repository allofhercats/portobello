import FormattedBase from "./FormattedBase";

abstract class FormattedVisitorBase {
    protected abstract onVisit(
        value: FormattedBase
    ): void;

    public visit(
        value: FormattedBase
    ): void {
        this.onVisit(value);
    }
}

export default FormattedVisitorBase;
