import FormattedBase from "./FormattedBase";
import FormattedElementBase from "./FormattedElementBase";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedSegmentTableColumn extends FormattedBase {
    public constructor(
        public titleElements: FormattedElementBase[] = [],
        public width: Number | null = null
    ) {
        super();
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {
        this.titleElements.forEach(i => i.accept(visitor));
    }
}

export default FormattedSegmentTableColumn;
