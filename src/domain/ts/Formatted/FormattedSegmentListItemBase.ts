import FormattedBase from "./FormattedBase";

abstract class FormattedSegmentListItemBase extends FormattedBase {
    public constructor(
        public value: Number | Boolean | null
    ) {
        super();
    }
};

export default FormattedSegmentListItemBase;
