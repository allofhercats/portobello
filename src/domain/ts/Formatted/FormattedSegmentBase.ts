import FormattedBase from "./FormattedBase";

abstract class FormattedSegmentBase extends FormattedBase {}

export default FormattedSegmentBase;
