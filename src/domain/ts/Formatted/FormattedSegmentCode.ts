import FormattedSegmentBase from "./FormattedSegmentBase";
import FormattedVisitorBase from "./FormattedVisitorBase";
import FormattedElementStringCode from "./FormattedElementStringCode";

class FormattedSegmentQuote extends FormattedSegmentBase {
    public constructor(
        public element: FormattedElementStringCode
    ) {
        super();
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {
        this.element.accept(visitor);
    }
}

export default FormattedSegmentQuote;
