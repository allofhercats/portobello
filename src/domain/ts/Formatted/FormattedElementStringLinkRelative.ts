import FormattedElementStringBase from "./FormattedElementStringBase";
import { FormattedElementStringSource } from "../Enums";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedElementStringLinkRelative extends FormattedElementStringBase {
    public constructor(
        source: FormattedElementStringSource,
        string: String,
        public pageId: Number
    ) {
        super(source, string);
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {}
};

export default FormattedElementStringLinkRelative;
