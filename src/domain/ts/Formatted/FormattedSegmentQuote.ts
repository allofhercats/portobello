import FormattedSegmentBase from "./FormattedSegmentBase";
import FormattedElementBase from "./FormattedElementBase";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedSegmentQuote extends FormattedSegmentBase {
    public constructor(
        public elements: FormattedElementBase[] = []
    ) {
        super();
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {
        this.elements.forEach(i => i.accept(visitor));
    }
}

export default FormattedSegmentQuote;
