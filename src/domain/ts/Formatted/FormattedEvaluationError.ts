class FormattedEvaluationError {
    constructor(
        public readonly message: String
    ) {}
}

export default FormattedEvaluationError;
