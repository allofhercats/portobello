import Color from "color";
import FormattedElementStringBase from "./FormattedElementStringBase";
import { FormattedElementStringSource } from "../Enums";
import FormattedVisitorBase from "./FormattedVisitorBase";

class FormattedElementStringText extends FormattedElementStringBase {
    public constructor(
        source: FormattedElementStringSource,
        string: String,
        public flagBold: Boolean = false,
        public flagItalic: Boolean = false,
        public flagUnderlined: Boolean = false,
        public flagStrikethrough: Boolean = false,
        public flagMonospace: Boolean = false,
        public colorForeground: Color | null = null,
        public colorBackground: Color | null = null
    ) {
        super(source, string);
    }

    protected onAccept(
        visitor: FormattedVisitorBase
    ): void {}
};

export default FormattedElementStringText;
