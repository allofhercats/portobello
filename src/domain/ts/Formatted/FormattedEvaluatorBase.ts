import { FormattedElementStringSource } from "../Enums";
import FormattedEvaluationError from "./FormattedEvaluationError";
import FormattedBase from "./FormattedBase";
import FormattedVisitorBase from "./FormattedVisitorBase";
import FormattedElementStringBase from "./FormattedElementStringBase";
import FormattedElementStringLinkExternal from "./FormattedElementStringLinkExternal";

abstract class FormattedEvaluatorBase extends FormattedVisitorBase {
    private errors: FormattedEvaluationError[] = [];

    protected abstract onEvaluateVariable(
        name: String
    ): [String, FormattedEvaluationError[]];

    private evaluate(
        source: FormattedElementStringSource,
        string: String
    ): String {
        if (source == FormattedElementStringSource.String) {
            return string;
        } else if (source == FormattedElementStringSource.Variable) {
            let stringEvaluated, errorsTmp;
            [stringEvaluated, errorsTmp] = this.onEvaluateVariable(string);
            this.errors.push(...errorsTmp);
            return stringEvaluated;
        } else {
            throw new Error(`Unexpected source for string element: ${source}`);
        }
    }

    protected onVisit(
        value: FormattedBase
    ): void {
        if (value instanceof FormattedElementStringBase) {
            value.stringEvaluated = this.evaluate(value.source, value.string);

            if (value instanceof FormattedElementStringLinkExternal) {
                value.stringURLEvaluated = this.evaluate(value.sourceURL, value.stringURL);
            }
        }
    }
}

export default FormattedEvaluatorBase;
