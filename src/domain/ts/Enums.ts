enum FormattedElementStringSource {
    String,
    Variable
}

enum Alignment {
    Left,
    Center,
    Right
}

enum FormattedSegmentListKind {
    Bullet,
    Numbered,
    Task
};

export {
    FormattedElementStringSource,
    Alignment,
    FormattedSegmentListKind
};
