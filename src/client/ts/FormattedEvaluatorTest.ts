import FormattedEvaluatorBase from "../../domain/ts/Formatted/FormattedEvaluatorBase";
import FormattedEvaluationError from "../../domain/ts/Formatted/FormattedEvaluationError";

class FormattedEvaluatorTest extends FormattedEvaluatorBase {
    protected onEvaluateVariable(
        name: String
    ): [String, FormattedEvaluationError[]] {
        return [
            "",
            [
                new FormattedEvaluationError(`No such variable: '${name}'`)
            ]
        ];
    }
}

export default FormattedEvaluatorTest;
