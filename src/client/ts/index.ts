import Vue from "vue";
import { FormattedSegmentListKind } from "../../domain/ts/Enums";
import FormattedElementStringText from "../../domain/ts/Formatted/FormattedElementStringText";
import FormattedElementStringCode from "../../domain/ts/Formatted/FormattedElementStringCode";
import FormattedElementStringFormula from "../../domain/ts/Formatted/FormattedElementStringFormula";
import FormattedElementStringImage from "../../domain/ts/Formatted/FormattedElementStringImage";
import FormattedElementStringRawHTML from "../../domain/ts/Formatted/FormattedElementStringRawHTML";
import FormattedSegmentText from "../../domain/ts/Formatted/FormattedSegmentText";
import FormattedSegmentHeading from "../../domain/ts/Formatted/FormattedSegmentHeading";
import FormattedSegmentTable from "../../domain/ts/Formatted/FormattedSegmentTable";
import FormattedSegmentCode from "../../domain/ts/Formatted/FormattedSegmentCode";
import FormattedSegmentList from "../../domain/ts/Formatted/FormattedSegmentList";
import FormattedSegmentListItemLeaf from "../../domain/ts/Formatted/FormattedSegmentListItemLeaf";
import FormattedSegmentListItemNode from "../../domain/ts/Formatted/FormattedSegmentListItemNode";
import FormattedSegmentQuote from "../../domain/ts/Formatted/FormattedSegmentQuote";
import FormattedSegmentSeparator from "../../domain/ts/Formatted/FormattedSegmentSeparator";
import PFormattedUnitRenderer from "../vue/Formatted/PFormattedUnitRenderer.vue";
import App from "../vue/App.vue";
import "typeface-open-sans";
import "source-code-pro/source-code-pro.css";
import "prismjs/themes/prism.css";
import "prismjs/components/prism-python";
import "katex/dist/katex.css";
import "../sass/index.scss";
import FormattedElementStringLinkExternal from "../../domain/ts/Formatted/FormattedElementStringLinkExternal";

document.addEventListener("DOMContentLoaded", () => {
    let divApp = document.createElement("div");
    divApp.setAttribute("id", "app");
    document.body.append(divApp);

    Vue.prototype.FormattedElementStringText = FormattedElementStringText;
    Vue.prototype.FormattedElementStringCode = FormattedElementStringCode;
    Vue.prototype.FormattedElementStringFormula = FormattedElementStringFormula;
    Vue.prototype.FormattedElementStringImage = FormattedElementStringImage;
    Vue.prototype.FormattedElementStringRawHTML = FormattedElementStringRawHTML;
    Vue.prototype.FormattedElementStringLinkExternal = FormattedElementStringLinkExternal;
    Vue.prototype.FormattedSegmentText = FormattedSegmentText;
    Vue.prototype.FormattedSegmentHeading = FormattedSegmentHeading;
    Vue.prototype.FormattedSegmentTable = FormattedSegmentTable;
    Vue.prototype.FormattedSegmentCode = FormattedSegmentCode;
    Vue.prototype.FormattedSegmentList = FormattedSegmentList;
    Vue.prototype.FormattedSegmentListKind = FormattedSegmentListKind;
    Vue.prototype.FormattedSegmentListItemLeaf = FormattedSegmentListItemLeaf;
    Vue.prototype.FormattedSegmentListItemNode = FormattedSegmentListItemNode;
    Vue.prototype.FormattedSegmentQuote = FormattedSegmentQuote;
    Vue.prototype.FormattedSegmentSeparator = FormattedSegmentSeparator;

    Vue.component("p-formatted-unit-renderer", PFormattedUnitRenderer);

    new Vue({
        el: "#app",
        render: (h) => h(App)
    });
});
